# Drone Cargo Dispatcher Service 

## Requirements
You must install JDK 17

## How to build
```shell
./gradlew clean build
```
if you give error "invalid source release" than add path param **-Dorg.gradle.java.home'**
```shell
./gradlew clean build -Dorg.gradle.java.home=/YOUR_JDK_17_PATH
```

## How to run locally
```shell
./gradlew bootRun
```

## How to send API request
Run application locally using instruction above.\
You can send request to API using Swagger or curl.

### Swagger
Open url below in your Web browser:\
**http://localhost:8080/swagger-ui/index.html**

### Curl
Open terminal window.\
Run command below:\
```shell
#Example request for Drone registration
curl --location --request POST 'localhost:8080/dispatcher/register' \
--header 'Content-Type: application/json' \
--data-raw '{
  "serialNumber": "SERIAL-NUMBER-11234",
  "model": "Lightweight",
  "weight": 500,
  "batteryCapacity": 100,
  "state": "IDLE"
 }
'
```