package ru.aleshin.dronecargo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.aleshin.dronecargo.AbstractTest;
import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.Medication;
import ru.aleshin.dronecargo.repository.DroneRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.aleshin.dronecargo.model.DroneModel.Heavyweight;
import static ru.aleshin.dronecargo.model.DroneState.IDLE;
import static ru.aleshin.dronecargo.model.DroneState.LOADING;

class DispatchControllerTest extends AbstractTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DroneRepository droneRepository;

    @Test
    void testRegisterPositive() throws Exception {
        Drone drone = Drone.builder()
                .serialNumber("test-drone-1")
                .batteryCapacity(100)
                .model(Heavyweight)
                .state(IDLE)
                .items(new ArrayList<>())
                .weight(500)
                .build();

        mockMvc.perform(post("/dispatcher/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(drone)))
                .andExpect(status().isOk());
    }

    @Test
    void testRegisterNegativeWhenWeightIsMoreThan500() throws Exception {
        Drone drone = Drone.builder()
                .serialNumber("test-drone-2")
                .batteryCapacity(100)
                .model(Heavyweight)
                .state(IDLE)
                .items(new ArrayList<>())
                .weight(501)
                .build();

        mockMvc.perform(post("/dispatcher/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(drone)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testLoadPositive() throws Exception {
        Drone drone = Drone.builder()
                .serialNumber("test-drone-3")
                .batteryCapacity(100)
                .model(Heavyweight)
                .state(IDLE)
                .items(new ArrayList<>())
                .weight(500)
                .build();

        drone = droneRepository.save(drone);

        Medication medication = Medication.builder()
                .code("TEST-MEDICATION-1")
                .weight(100)
                .image(new byte[0])
                .name("dsfsdf")
                .build();

        mockMvc.perform(post("/dispatcher/load")
                        .param("droneId", drone.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(List.of(medication))))
                .andExpect(status().isOk());
    }

    @Test
    void testCheckBatteryCapacityPositive() throws Exception {
        Drone drone = Drone.builder()
                .serialNumber("test-drone-4")
                .batteryCapacity(99)
                .model(Heavyweight)
                .state(IDLE)
                .items(new ArrayList<>())
                .weight(500)
                .build();

        drone = droneRepository.save(drone);

        mockMvc.perform(get("/dispatcher/drones/{droneId}/battery", drone.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(drone.getBatteryCapacity().toString()));
    }

    @Test
    void testGetDroneItemsPositive() throws Exception {
        Medication medication = Medication.builder()
                .code("TEST-MEDICATION-2")
                .weight(100)
                .image(new byte[0])
                .name("dsfsdf")
                .build();

        Drone drone = Drone.builder()
                .serialNumber("test-drone-5")
                .batteryCapacity(99)
                .model(Heavyweight)
                .state(IDLE)
                .items(List.of(medication))
                .weight(500)
                .build();

        drone = droneRepository.save(drone);

        mockMvc.perform(get("/dispatcher/drones/{droneId}/items", drone.getId()))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(drone.getItems())));
    }

    @Test
    void testGetDroneByStatePositive() throws Exception {
        Drone drone = Drone.builder()
                .serialNumber("test-drone-6")
                .batteryCapacity(99)
                .model(Heavyweight)
                .state(IDLE)
                .items(new ArrayList<>())
                .weight(500)
                .build();

        drone = droneRepository.save(drone);

        MvcResult result = mockMvc.perform(get("/dispatcher/drones/").param("droneStates", IDLE.name(), LOADING.name()))
                .andExpect(status().isOk())
                .andReturn();
        List<Drone> drones = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Drone>>() {
        });
        Set<String> serialNumbers = drones.stream().map(Drone::getSerialNumber).collect(Collectors.toSet());

        assertTrue(serialNumbers.contains(drone.getSerialNumber()));

    }
}