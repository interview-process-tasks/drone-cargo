package ru.aleshin.dronecargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DroneCargoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroneCargoApplication.class, args);
	}

}
