package ru.aleshin.dronecargo.exception;

public class DroneLowBatteryException extends RuntimeException{
    public DroneLowBatteryException(String capacity) {
        super(String.format("Battery level is below %s%%", capacity));
    }
}
