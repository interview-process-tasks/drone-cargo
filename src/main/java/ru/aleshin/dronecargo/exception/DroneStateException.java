package ru.aleshin.dronecargo.exception;

import ru.aleshin.dronecargo.model.DroneState;

import java.util.Arrays;

public class DroneStateException extends RuntimeException {
    public DroneStateException(DroneState currentState, DroneState... state) {
        super(String.format("Drone state %s is not allowed for this action. Allowed states: %s", currentState, Arrays.toString(state)));
    }
}
