package ru.aleshin.dronecargo.exception;

public class DroneWeightException extends RuntimeException {
    public DroneWeightException(Integer weightLimit, Integer calcWeight, Integer itemsWeight) {
        super(String.format("Weight limit exceeded. Available weight: %s. Items weight: %s", weightLimit - calcWeight, itemsWeight));
    }
}
