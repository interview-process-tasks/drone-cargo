package ru.aleshin.dronecargo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.DroneState;
import ru.aleshin.dronecargo.model.Medication;
import ru.aleshin.dronecargo.repository.DroneRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DronesServiceImpl implements DronesService {

    private final DroneRepository droneRepository;

    @Override
    public Integer getDroneBatteryCapacity(Long droneId) {
        return droneRepository.findById(droneId)
                .orElseThrow()
                .getBatteryCapacity();
    }

    @Override
    public List<Medication> getDroneItems(Long droneId) {
        return droneRepository.findById(droneId)
                .orElseThrow()
                .getItems();
    }

    @Override
    public List<Drone> getDronesByState(DroneState... droneStates) {
        return droneRepository.findByStateIn(List.of(droneStates));
    }
}
