package ru.aleshin.dronecargo.service;

import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.DroneState;
import ru.aleshin.dronecargo.model.Medication;

import java.util.List;

public interface DronesService {

    Integer getDroneBatteryCapacity(Long droneId);

    List<Medication> getDroneItems(Long droneId);

    List<Drone> getDronesByState(DroneState... droneStates);

}
