package ru.aleshin.dronecargo.service;

import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.Medication;

import java.util.List;

public interface DispatcherService {
    void register(Drone drone);

    void load(Long droneId, List<Medication> medications);

}
