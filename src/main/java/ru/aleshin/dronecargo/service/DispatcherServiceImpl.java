package ru.aleshin.dronecargo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.aleshin.dronecargo.exception.DroneLowBatteryException;
import ru.aleshin.dronecargo.exception.DroneStateException;
import ru.aleshin.dronecargo.exception.DroneWeightException;
import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.DroneState;
import ru.aleshin.dronecargo.model.Medication;
import ru.aleshin.dronecargo.repository.DroneRepository;

import java.util.Arrays;
import java.util.List;

import static ru.aleshin.dronecargo.model.DroneState.*;

@Service
@RequiredArgsConstructor
public class DispatcherServiceImpl implements DispatcherService {

    private final DroneRepository droneRepository;
    private static final int MIN_BATTERY_CAPACITY = 25;

    @Override
    public void register(Drone drone) {
        drone.setState(IDLE);
        droneRepository.save(drone);
    }

    @Override
    public void load(Long droneId, List<Medication> medications) {
        Drone drone = droneRepository.findById(droneId).orElseThrow();

        checkState(drone.getState(), IDLE, LOADING);

        checkBattery(drone.getBatteryCapacity());

        drone.setState(LOADING);
        droneRepository.save(drone);
        Integer calcWeight = drone.getItems().stream()
                .mapToInt(Medication::getWeight)
                .sum();
        Integer itemsWeight = medications.stream().mapToInt(Medication::getWeight).sum();

        checkWeight(drone.getWeight(), calcWeight, itemsWeight);

        drone.getItems().addAll(medications);
        drone.setState(LOADED);
        droneRepository.save(drone);
    }

    private void checkWeight(Integer weightLimit, Integer calcWeight, Integer itemsWeight) {
        if ((weightLimit - calcWeight - itemsWeight) < 0) {
            throw new DroneWeightException(weightLimit, calcWeight, itemsWeight);
        }
    }

    private void checkState(DroneState currentState, DroneState... allowStates) {
        Arrays.stream(allowStates)
                .filter(state -> state.equals(currentState))
                .findAny()
                .orElseThrow(() -> new DroneStateException(currentState, allowStates));
    }

    private void checkBattery(Integer batteryCapacity) {
        if (batteryCapacity < MIN_BATTERY_CAPACITY) {
            throw new DroneLowBatteryException(String.valueOf(MIN_BATTERY_CAPACITY));
        }
    }
}
