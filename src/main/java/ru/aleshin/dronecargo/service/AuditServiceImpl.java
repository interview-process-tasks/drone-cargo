package ru.aleshin.dronecargo.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.aleshin.dronecargo.repository.DroneRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuditServiceImpl implements AuditService {

    private final DroneRepository droneRepository;

    @Override
    @Scheduled(fixedDelay = 1000)
    public void batteryAudit() {
        log.info("Audit is started");
        droneRepository.findAll().forEach(drone -> log.info("Drone ID: {}. Audit battery capacity - {}.", drone.getId(), drone.getBatteryCapacity()));
        log.info("Audit is finished");
    }
}
