package ru.aleshin.dronecargo.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.DroneState;
import ru.aleshin.dronecargo.model.Medication;
import ru.aleshin.dronecargo.service.DispatcherService;
import ru.aleshin.dronecargo.service.DronesService;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/dispatcher")
public class DispatchController {

    private final DispatcherService dispatcherService;
    private final DronesService dronesService;

    @PostMapping("/register")
    public void register(@Valid @RequestBody Drone drone) {
        dispatcherService.register(drone);
    }

    @PostMapping("/load")
    public void load(@RequestParam Long droneId,@Valid @RequestBody List<Medication> medications) {
        dispatcherService.load(droneId, medications);
    }

    @GetMapping("/drones/{droneId}/battery")
    public Integer checkBatteryCapacity(@PathVariable("droneId") Long droneId) {
        return dronesService.getDroneBatteryCapacity(droneId);
    }

    @GetMapping("/drones/{droneId}/items")
    public List<Medication> getDroneItems(@PathVariable("droneId") Long droneId) {
        return dronesService.getDroneItems(droneId);
    }

    @GetMapping("/drones/")
    public List<Drone> getDroneByState(@RequestParam DroneState... droneStates) {
        return dronesService.getDronesByState(droneStates);
    }
}
