package ru.aleshin.dronecargo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.aleshin.dronecargo.exception.DroneLowBatteryException;
import ru.aleshin.dronecargo.exception.DroneStateException;
import ru.aleshin.dronecargo.exception.DroneWeightException;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
            ConstraintViolationException.class,
    })
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException constraintViolationException) {
        Map<String, Object> body = createBody(Arrays.toString(constraintViolationException.getConstraintViolations().toArray()));
        return ResponseEntity.badRequest()
                .body(body);
    }

    @ExceptionHandler({
            org.hibernate.exception.ConstraintViolationException.class
    })
    public ResponseEntity<Object> handleHibernateConstraintViolationException(org.hibernate.exception.ConstraintViolationException constraintViolationException) {
        Map<String, Object> body = createBody(constraintViolationException.getConstraintName());
        return ResponseEntity.badRequest()
                .body(body);
    }

    @ExceptionHandler({
            DroneLowBatteryException.class,
            DroneStateException.class,
            DroneWeightException.class
    })
    public ResponseEntity<Object> handleDroneException(Exception exception) {
        Map<String, Object> body = createBody(exception.getMessage());
        return ResponseEntity.badRequest()
                .body(body);
    }

    private Map<String, Object> createBody(String message) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        body.put("message", message);
        return body;
    }
}
