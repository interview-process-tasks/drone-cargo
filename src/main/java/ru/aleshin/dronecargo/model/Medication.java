package ru.aleshin.dronecargo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Medication {

    @Id
    @GeneratedValue
    private Long id;

    @Pattern(regexp = "^[a-zA-Z0-9-_]+$", message = "allowed only letters, numbers, ‘-‘, ‘_’")
    private String name;

    private Integer weight;

    @Pattern(regexp = "^[A-Z0-9_]+$", message = "allowed only upper case letters, underscore and numbers")
    private String code;

    private byte[] image;
}
