package ru.aleshin.dronecargo.model;

public enum DroneState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
}
