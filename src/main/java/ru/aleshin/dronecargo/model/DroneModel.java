package ru.aleshin.dronecargo.model;

public enum DroneModel {
    Lightweight, Middleweight, Cruiserweight, Heavyweight;
}
