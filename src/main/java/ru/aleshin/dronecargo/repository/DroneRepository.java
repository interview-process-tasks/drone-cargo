package ru.aleshin.dronecargo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aleshin.dronecargo.model.Drone;
import ru.aleshin.dronecargo.model.DroneState;

import java.util.List;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
    List<Drone> findByStateIn(List<DroneState> states);
}
